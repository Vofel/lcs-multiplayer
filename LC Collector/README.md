
# Liberty City Collector

## Description

*Find and collect as many packages as possible. The harder the location is, the more points you get! Do everything to
prevent others from collecting more than you.*

In this custom game mode you have to collect hidden packages. The locations are different from single player. Some
of them require using various vehicles or performing stunts to get to them. Others are only available in the "Team
vs Team" mode, because they are very hard to reach on your own, if not downright impossible. You'll get more points
for harder packages.

**Version:** 1.4

**Game over conditions:**

- No packages are left
- Time is up
- There are not enough points left in game to catch up to the leader

**Known issues:**
- the original pickup range is extremely large and that's unacceptable, so you must run through a package
to collect it. Otherwise, you won't receive any points, and the package will respawn in 30 seconds. Make
sure you see a message confirming that you got the points before continuing with your search!

## Installation

Open your game .iso in UMDGen (v4.00) and navigate to `PSP_GAME -> USRDIR -> LUASCRIPTS`. Rename `PAKKOTS.LUA.LC`
to `DEATHMATCH.LUA.LC` and replace it in the aforementioned directory. If you're planning to use hashes (more 
about them later), you'll also need to install the `STATESAVER` module. To do so simply put
[STATESAVER.LUA.LC](../Utils/STATESAVER.LUA.LC) in the same directory (`LUASCRIPTS`). Save the game under a new
name. In game choose the "Liberty City Survivor" game mode. The settings and their meaning are the following:

- Game Location: the island you're going to play on
- Play Cutscene: package model to use. "Yes" - the original, "No" - package from GTA III (harder to spot),
"Only once" - the first match the model is going to be the original, the next ones - from GTA III
- Game Style: "Free for all" or "Gang war" ("Team vs Team")
- Kill Limit: maximum number of packages to spawn
- Time Limit: maximum length of the game. The value is going to be multiplied by 3
- Powerups: currently unused
- Character: your character

There are extra settings you can specify directly in `PAKKOTS.LUA`. If set, they will overwrite what the host
chooses in the lobby. The installation process for this file is the same as above, except you rename `PAKKOTS.LUA`
to `DEATHMATCH.LUA.LC`. The settings and their meaning are the following:

- `NUM_PACKAGES`:   number of packages to spawn. If set, the "Kill Limit" setting will be ignored.
                    Default: `nil` (unset)
- `HIDE_PLAYERS`:   hide opponents on the minimap if they are too far away. Doesn't overwrite anything. Default: `true`
- `CALCULATE_HASH`: calculate and show the game state hash, so you can restart the game later from
                  where you've left it. Useful if somebody loses connection during a session. Default: `true`
				  **IMPORTANT:** make sure to read about this option in the `Using hashes` section
- `STATE_HASH`:     state hash string to continue the game. This option is ignored if `CALCULATE_HASH`
                  isn't set to `true`. If set to `nil`, starts a new game.
                  Allowed non-ASCII characters: `£`, `¥`, `§`, `¿`. Default: nil

## Using hashes

The main issue of LCS Multiplayer is players losing connection. If that happens, there's no way to join the game back.
Since this game mode is meant to be played for a prolonged period of time, these disconnects can be quite devastating.
To overcome this problem, a state saving system has been implemented. Currently, it's capable of saving the remaining
on the map packages, so you can stop the game and continue from the same point later. It doesn't save players'
scores - this will be added in future updates.

The system works by generating and displaying a hash - a unique string of some length. You can write it down and
restore the state by putting it in the `STATE_HASH` variable. Hashes consist of regular ASCII characters plus 4 extra
ones: `£`, `¥`, `§`, `¿`. They are provided here, so you can conveniently copy and paste them. Some characters in
GTA: Liberty City Stories look the same. For example, `0` and `O`, `I` and `l`, `` ` `` and `'`. You can use any
of them without affecting the hash.

Only the host needs to activate the system - other players won't see the hash anyway. The system is activated when
`CALCULATE_HASH` is set to true, which is the default value. Once that's done, the hash should appear on the screen
in-game. It is updated after someone collects a package. Make sure the `STATESAVER` module is installed!

## Changelog

Check [PAKKOTS.LUA](./LC Collector/PAKKOTS.LUA)
